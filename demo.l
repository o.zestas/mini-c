%option noyywrap yylineno

%{
#include <stdio.h>
#include <stdlib.h>
#include "demo.tab.h"
#include "Tree.h"
%}

%%

"function" { return FUNCTION; }
"return" { return RETURN; }
"sqrt" { return SQRT; }
"pow" { return POW; }
"+" { return PLUS; } 
"-" { return MINUS; } 
"/" { return DIV; } 
"*" { return MULT; } 
"||" { return OR; }
"&&" { return AND; }
"!" { return NOT; }
"!=" { return NEQUAL; }
"==" { return EQUAL; }
"<" { return LT; }
">" { return GT; }
"<=" { return LTE; }
">=" { return GTE; }

";" { return ';';}
"(" { return '('; }
")" { return ')'; }
"{" { return '{'; }
"}" { return '}'; }
"," { return ','; }

"=" { return '='; }

"if" { return IF; }
"else" { return ELSE; }
"while" { return WHILE; }

((0|[1-9][0-9]*)?\.0|[1-9][0-9]*)|(0|[1-9][0-9]*\.(0|[1-9][0-9]*)?) {	yylval.node = NewNode(NT_NUMBER,NULL,NULL,NULL,yytext);
																	return NUMBER; }

[a-zA-Z][0-9a-zA-Z_]*	{	yylval.node = getSymbol(yytext);
							return IDENTIFIER; }

[ \t\n\r]+ ;
 

%%
